# Events

## How to Start with docker

First of all you have to install npm dependencies:
```console
$ npm i
```

and make `.env` file:
```console
$ cp .env.example .env
```

This file contains ENV variables for correct work of the service.

### Command to add migrations to DB
```console
$ docker-compose run api npm run db:migrate:up
```

### Command to remove all tables from DB
```console
$ docker-compose run api npm run db:migrate:down
```

### Command to add test data into DB
```console
$ docker-compose run api npm run db:seed:up
```

### Start development environment
```console
$ docker-compose up --build
```
> Please note that `--build` is optional but yet recommended to ensure fresh `node_modules`

### Start fresh development environment
```console
$ docker-compose rm -f && docker-compose up
```

### Command to execute all integration tests
```console
$ docker-compose run api npm run integration
```
> Make sure you started the service using the command above before testing

## How to start without Docker
Also, you can run the service without docker.
Just use the following command:
```console
$ npm run dev
```

instead of [docker command](#start-development-environment)

Before this command you need to add DB credentials to `.env`