const assert = require('chai').assert;
const config = require('config');
const request = require('supertest');

const serviceUrl = config.service.serviceUrl;

describe('Events service', () => {
  it('will request /health resource and verify response', async () => {
    const { body, statusCode } = await request(serviceUrl).get('/health');
    assert.equal(statusCode, 200);
    assert.equal(body.status, 'OK');
    assert.equal(body.message, 'Service is up and running');
  });

  it('will request not existing resource and verify response', async () => {
    const { statusCode } = await request(serviceUrl).get('/not-existing-resource');
    assert.equal(statusCode, 404);
  });
});
