module.exports = [{
  id: 1,
  eventId: 1,
  ticketTypeId: 1,
  option: 'multiple',
  quantity: 317,
  price: 2500
}, {
  id: 2,
  eventId: 1,
  ticketTypeId: 2,
  option: 'multiple',
  quantity: 254,
  price: 1700
}, {
  id: 3,
  eventId: 1,
  ticketTypeId: 3,
  option: 'avoid one',
  quantity: 54,
  price: 1800
}, {
  id: 4,
  eventId: 2,
  ticketTypeId: 4,
  option: 'altogether',
  quantity: 50,
  price: 900
}, {
  id: 5,
  eventId: 3,
  ticketTypeId: 1,
  option: 'multiple',
  quantity: 100,
  price: 1300
}, {
  id: 6,
  eventId: 4,
  ticketTypeId: 4,
  option: 'avoid one',
  quantity: 50,
  price: 700
}];
