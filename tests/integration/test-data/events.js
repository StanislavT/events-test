const moment = require('moment');

module.exports = [{
  id: 1,
  name: 'Test event',
  address: 'Address of test event',
  gathering: moment().utc().add(10, 'days').toISOString()
}, {
  id: 2,
  name: 'Second test event',
  address: 'Address of second test event',
  gathering: moment().utc().add(15, 'days').toISOString()
}, {
  id: 3,
  name: 'Expired event',
  address: 'Address of expired event',
  gathering: moment().utc().subtract(15, 'days').toISOString()
}];
