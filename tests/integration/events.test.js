const assert = require('chai').assert;
const config = require('config');
const request = require('supertest');
const db = require('../../src/db');

db.sequelize.options.logging = () => null;

const serviceUrl = config.service.serviceUrl;

const events = require('./test-data/events');
const ticketTypes = require('./test-data/ticket-type');
const tickets = require('./test-data/tickets');

describe('Events service: Events', () => {
  before(async() => {
    await db.Events.truncate();
    await db.Orders.truncate();
    await db.TicketTypes.truncate();
    await db.Tickets.truncate();
  });

  describe('When db is empty', () => {
    before(async() => {
      await db.Events.truncate();
    });

    it('`GET /events` returns empty array', async() => {
      const data = await request(serviceUrl).get('/events');

      assert.equal(data.statusCode, 200);
      assert.isArray(data.body);
      assert.lengthOf(data.body, 0);
    });
  });

  describe('When db contains single active record', () => {
    before(async() => {
      await db.Events.truncate();
      await db.Events.create(events[0]);
    });

    it('`GET /events` returns collection with single item', async() => {
      const data = await request(serviceUrl).get('/events');

      assert.equal(data.statusCode, 200);
      assert.isArray(data.body);
      assert.lengthOf(data.body, 1);
      assert.deepEqual(data.body[0], events[0])
    });

    it('`GET /events/1` returns single item', async() => {
      const data = await request(serviceUrl).get('/events/1');

      assert.equal(data.statusCode, 200);
      assert.isObject(data.body);
      assert.deepEqual(data.body, events[0])
    });
  });

  describe('When db contains two active and one inactive records', () => {
    before(async() => {
      await db.Events.truncate();
      await db.Events.bulkCreate(events);
    });

    it('`GET /events` returns only active events', async() => {
      events.length = 2; // Remove expired event

      const data = await request(serviceUrl).get('/events');

      assert.equal(data.statusCode, 200);
      assert.isArray(data.body);
      assert.lengthOf(data.body, 2);
      assert.deepEqual(data.body, events)
    });

    it('Request to inactive event returns 404', async() => {
      const data = await request(serviceUrl).get('/events/3');

      assert.equal(data.statusCode, 404);
    });
  });

  describe('When active event has tickets', () => {
    before(async() => {
      await db.Events.truncate();
      await db.Events.bulkCreate(events);
      await db.TicketTypes.bulkCreate(ticketTypes);
      await db.Tickets.bulkCreate(tickets);
    });

    it('`GET /events/1/tickets` returns available tickets', async() => {
      const expected = [{
        id: 1,
        option: 'multiple',
        totalQuantity: 317,
        reservedQuantity: 0,
        forSaleQuantity: 317,
        price: 2500,
        type: 'Amphitheater'
      }, {
        id: 2,
        option: 'multiple',
        totalQuantity: 254,
        reservedQuantity: 0,
        forSaleQuantity: 254,
        price: 1700,
        type: 'Balcony'
      }, {
        id: 3,
        option: 'avoid one',
        totalQuantity: 54,
        reservedQuantity: 0,
        forSaleQuantity: 54,
        price: 1800,
        type: 'Box'
      }];

      const data = await request(serviceUrl).get('/events/1/tickets');

      assert.equal(data.statusCode, 200);
      assert.isArray(data.body);
      assert.lengthOf(data.body, 3);
      assert.deepEqual(data.body, expected)
    });

    it('`GET /events/1/tickets` after reservation returns available tickets', async() => {
      const expected = [{
        id: 1,
        option: 'multiple',
        totalQuantity: 317,
        reservedQuantity: 0,
        forSaleQuantity: 317,
        price: 2500,
        type: 'Amphitheater'
      }, {
        id: 2,
        option: 'multiple',
        totalQuantity: 254,
        reservedQuantity: 3,
        forSaleQuantity: 251,
        price: 1700,
        type: 'Balcony'
      }, {
        id: 3,
        option: 'avoid one',
        totalQuantity: 54,
        reservedQuantity: 0,
        forSaleQuantity: 54,
        price: 1800,
        type: 'Box'
      }];

      await request(serviceUrl).post('/orders')
        .send({
          userId: 1,
          ticketId: 2,
          quantity: 3
        });
      const data = await request(serviceUrl).get('/events/1/tickets');

      assert.equal(data.statusCode, 200);
      assert.isArray(data.body);
      assert.lengthOf(data.body, 3);
      assert.deepEqual(data.body, expected)
    });
  });
});
