module.exports = {
  service: {
    port: process.env.SERVICE_PORT,
    serviceUrl: process.env.SERVICE_URL
  },
  bodyParser: {
    limit: '1mb'
  },
  db: {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    dialect: 'postgres'
  },
  logger: {
    level: process.env.LOG_LEVEL || 'info'
  }
};
