const { level } = require('config').get('logger');
const winston = require('winston');

winston.configure({
  level,
  transports: [
    new winston.transports.Console({
      colorize: true,
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.colorize(),
        winston.format.simple()
      ),
    })
  ]
});

module.exports = winston;
