const router = require('express').Router();
const handler = require('../handler');
const { version } = require('../../../package.json');

router.get('/', handler(() => {
  return {
    status: 'OK',
    message: 'Service is up and running',
    build: version || 'unknown',
    uptime: process.uptime()
  }
}));

module.exports = router;
