const router = require('express').Router();
const handler = require('../handler');
const db = require('../../db');

router.get('/', handler(() => {
  return db.repository.Events.activeEvents()
}));

router.get('/:id', handler((req) => {
  const id = req.param('id');

  return db.repository.Events.activeEvent(id)
}));

router.get('/:id/tickets', handler((req) => {
  const id = req.param('id');

  return db.repository.Events.eventTickets(id)
}));

module.exports = router;
