const router = require('express').Router();
const moment = require('moment');
const handler = require('../handler');
const db = require('../../db');
const logger = require('../../logger');
const payment = require('../../payment');

router.post('/', handler((req) => {
  const { ticketId, quantity } = req.body;

  return db.repository.Tickets.activeTicket(ticketId)
    .then(ticket => {

      if (ticket.forSaleQuantity < quantity) {
        return { message: `You cannot buy more than ${ticket.forSaleQuantity} tickets` }
      }

      switch (ticket.option) {
        case 'altogether':
          if (ticket.forSaleQuantity !== quantity) {
            return { message: 'This kind of tickets must be bought all together' }
          }

          break;
        case 'multiple':
          if (quantity < 2) {
            return { message: 'You have to buy at least two tickets' }
          }

          break;
        case 'avoid one':
          if (ticket.forSaleQuantity - quantity === 1) {
            return { message: 'Unfortunately, you cannot leave only the one ticket of this ticket type' }
          }

          break;
      }

      const data = {
        userId: 1, // It can be taken from JWT or somehow in another way. This is not a problem of this microservice.
        ticketId,
        totalPrice: ticket.price * quantity,
        quantity,
        status: 'created'
      };

      return db.Orders.create(data);
    });
}));

router.get('/:id', handler((req) => {
  const id = req.param('id');

  return db.Orders.findById(id);
}));

router.post('/:id/pay', handler((req) => {
  const id = req.param('id');

  return db.Orders.findById(id)
    .then(order => {
      if (!order) {
        return;
      }

      logger.debug('Order info', order.get({ plain: true }));

      switch (order.status) {
        case 'created':
          if(isNotOutdated(order)) {
            return payment.charge(order.totalPrice, 'success')
              .then(payment => {
                order.status = 'purchased';
                order.paymentToken = payment.paymentToken;

                return order.save();
              })
              .then(order => db.Tickets.findById(order.ticketId))
              .then(ticket => {
                ticket.quantity -= order.quantity;

                return ticket.save().then(() => order)
              });
          } else {
            return { message: 'This order is expired' };
          }

          break;
        case 'cancelled':
          return { message: 'This order has been cancelled' };
        case 'expired':
          return { message: 'This order is expired' };
        case 'purchased':
          return { message: 'This order has been successfully purchased', when: order.updatedAt };
      }
    })
}));

module.exports = router;

function isNotOutdated(data) {
  return moment(data.createdAt).utc().isBetween(moment().utc().subtract(15, 'minutes'), moment().utc())
}