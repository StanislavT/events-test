const { port } = require('config').get('service');
const express = require('express');
const logger = require('../logger');

module.exports.start = async () => {
  const app = express();

  const packageJson = require('../../package.json');
  const swagger = await require('./swagger')({ packageJson });
  require('./middleware/')({ app, swagger, logger });

  app.listen(port, () =>
    logger.info('Server is up and running', { port }));
};
