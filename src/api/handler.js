module.exports = action => (req, res, next) =>
  Promise.resolve()
    .then(() => action(req, res))
    .then((result) => {
      if (result) {
        res.status(200).json(result);
      } else {
        res.boom.notFound();
      }
    })
    .catch(next);
