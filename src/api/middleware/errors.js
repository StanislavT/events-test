class NotFound extends Error {}

module.exports = ({ app, logger }) => {
  app.use((req, res, next) => {
    next(new NotFound('Not found'));
  });

  app.use((error, req, res, next) => {
    const { message, name, stack, statusCode } = error;
    if (statusCode) {
      statusCode >= 500
      ? logger.error(message, { stack })
      : logger.warn(message);
      const boomError = res.boom.create(statusCode, message).output.payload;
      res.status(statusCode).send(boomError);
    } else if (name === 'UnauthorizedError') {
      logger.warn(message);
      res.boom.unauthorized(error);
    } else if (error instanceof NotFound) {
      res.boom.notFound();
    } else {
      logger.error(message, { stack });
      res.boom.internal();
    }

    next();
  });
};
