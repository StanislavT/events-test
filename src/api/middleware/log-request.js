const onFinished = require('on-finished');

const getMilliseconds = time => Math.floor(((time[0] * 1e9) + time[1]) / 1e6);

module.exports = ({ app, logger }) => {
  app.use((req, res, next) => {
    req._hrstart = process.hrtime();
    onFinished(res, () => {
      const responseTime = `${getMilliseconds(process.hrtime(req._hrstart))}ms`;
      const status = res.statusCode;
      const data = {
        responseTime,
        status
      };

      const msg = `${req.method} ${req.originalUrl}`;
      if (status >= 500) logger.error(msg, data);
      else if (status >= 400) logger.warn(msg, data);
      else if (status >= 200) logger.info(msg, data);
      else logger.debug(msg, data);
    });
    next();
  });
};
