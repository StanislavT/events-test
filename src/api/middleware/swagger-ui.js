const swaggerUi = require('swagger-ui-express');

module.exports = ({ app, swagger }) => {
  app.get('/', (req, res) => {
    res.redirect('/documentation');
  });

  const options = {
    validatorUrl: null,
    docExpansion: 'list'
  };
  app.use('/documentation', swaggerUi.serve,
    swaggerUi.setup(swagger, true, options));
};
