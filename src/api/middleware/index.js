module.exports = options => [
  require('./log-request'),
  require('./body-parser'),
  require('./boom'),
  require('./handlers'),
  require('./swagger-ui'),
  require('./errors')
].map(factory => factory(options));
