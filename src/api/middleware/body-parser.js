const config = require('config');
const bodyParser = require('body-parser');

module.exports = ({ app }) => {
  app.use(bodyParser.json(config.bodyParser));
};
