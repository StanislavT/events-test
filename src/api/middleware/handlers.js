const events = require('../handlers/events');
const health = require('../handlers/health');
const orders = require('../handlers/orders');

module.exports = ({ app }) => {
  app.use('/health', health);
  app.use('/events', events);
  app.use('/orders', orders);
};
