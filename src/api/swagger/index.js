const { join } = require('path');
const parser = require('swagger-parser');

module.exports = async ({ packageJson }) => {
  const swagger = await parser.validate(join(__dirname, './index.yaml'));

  const { version, description, author } = packageJson;
  swagger.info = {
    title: description,
    version,
    contact: author
  };

  return swagger;
};
