const joi = require('joi');

module.exports = {
  id: joi.string().guid().required()
};
