const joi = require('joi');
const uuid = require('uuid');

module.exports = {
  id: joi.string().guid({ version: 'uuidv4' }).default(() => uuid.v4(), 'new UUID')
};
