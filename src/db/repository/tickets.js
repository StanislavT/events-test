module.exports = (db) => ({
  /**
   * Returns tickets for an event by id
   * @param {integer} id - Event id
   */
  activeTicket: (id) => db.Tickets.findById(id, {
    attributes: { exclude: ['eventId', 'ticketTypeId'] },
    include: [{
      association: 'reservedTickets',
      attributes: [ 'id', 'quantity', 'createdAt'],
      required: false
    }],
    required: false
  })
  /**
   * This aggregation can be implemented on DB side as well.
   * For example as View in PostgreSQL.
   * It will help us to get a calculated date from DB.
   */
    .then(result => {
      if(result) {
        const ticket = result.get({ plain: true });

        const { id, option, quantity: totalQuantity, price } = ticket;
        const reservedQuantity = ticket.reservedTickets.reduce((acc, item) => acc + item.quantity, 0);
        const forSaleQuantity = totalQuantity - reservedQuantity;

        return { id, option, totalQuantity, reservedQuantity, forSaleQuantity, price };
      }

      return;
    })
});
