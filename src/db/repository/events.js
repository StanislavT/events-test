module.exports = (db) => ({
  /**
   * Returns active events
   */
  activeEvents: () => db.Events.findAll({
    where: {
      gathering: {
        [db.sequelize.Op.gt]: new Date()
      }
    }
  }),
  /**
   * Returns active event
   * @param {integer} id - Event id
   */
  activeEvent: (id) => db.Events.findOne({
    where: {
      id,
      gathering: {
        [db.sequelize.Op.gt]: new Date()
      }
    }
  }),
  /**
   * Returns tickets for an event by id
   * @param {integer} id - Event id
   */
  eventTickets: (id) => db.Events.findOne({
    where: {
      id,
      gathering: {
        [db.sequelize.Op.gt]: new Date()
      },
    },
    include: [{
      where: {
        quantity: {
          [db.sequelize.Op.gt]: 0
        }
      },
      association: 'tickets',
      attributes: { exclude: ['eventId', 'ticketTypeId'] },
      include: [{
        association: 'ticketType',
        attributes: [ 'name' ],
        required: false
      }, {
        association: 'reservedTickets',
        attributes: [ 'id', 'quantity', 'createdAt'],
        required: false
      }],
      required: false
    }]
  })
  /**
   * This aggregation can be implemented on DB side as well.
   * For example as View in PostgreSQL.
   * It will help us to get a calculated date from DB.
   */
    .then(result => {
      if(result) {
        return result.get({ plain: true }).tickets.map(ticket => {
          const { id, option, quantity: totalQuantity, price, ticketType: { name: type },  } = ticket;
          const reservedQuantity = ticket.reservedTickets.reduce((acc, item) => acc + item.quantity, 0);
          const forSaleQuantity = totalQuantity - reservedQuantity;

          return { id, option, totalQuantity, reservedQuantity, forSaleQuantity, price, type };
        });
      }

      return;
    })
});
