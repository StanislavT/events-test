const Sequelize = require('sequelize');
const { host, port, database, dialect, username, password } = require('config').get('db');
const logger = require('../logger');

const options = {
  host,
  port,
  database,
  username,
  password,
  dialect,
  logging: logger.debug,
  freezeTableName: true,
  operatorsAliases: false,
  dialectOptions: {
    requestTimeout: 5000
  }
};

const sequelize = new Sequelize(options);

/**
 * Check connection to DB
 */
sequelize.authenticate()
  .then(() => {
    logger.info('DB connection has been established successfully');
  })
  .catch((err) => {
    logger.error('unable to connect to the database', err);
  });

const db = {
  Events: sequelize.import('./models/events'),
  Orders: sequelize.import('./models/orders'),
  Tickets: sequelize.import('./models/tickets'),
  TicketTypes: sequelize.import('./models/ticket-type'),
};

Object.keys(db).forEach((key) => {
  if ('associate' in db[key]) {
    db[key].associate(db);
  }
});

db.sequelize = sequelize;

db.repository = {
  Events: require('./repository/events')(db),
  Tickets: require('./repository/tickets')(db)
};

module.exports = db;
