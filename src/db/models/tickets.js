module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Tickets', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    eventId: {
      field: 'event_id',
      type: DataTypes.INTEGER
    },
    ticketTypeId: {
      field: 'ticket_type_id',
      type: DataTypes.INTEGER
    },
    option: {
      type: DataTypes.ENUM,
      values: ['multiple', 'avoid one', 'altogether']
    },
    quantity: {
      type: DataTypes.INTEGER
    },
    price: {
      type: DataTypes.INTEGER
    }
  }, {
    timestamps: false
  });

  Model.associate = (models) => {
    Model.belongsTo(models.Events, {
      foreignKey: 'eventId',
      as: 'event'
    });

    Model.hasOne(models.TicketTypes, {
      foreignKey: 'id',
      targetKey: 'ticketTypeId',
      as: 'ticketType'
    });

    Model.hasMany(models.Orders, {
      foreignKey: 'ticketId',
      as: 'orders'
    });

    Model.hasMany(models.Orders.scope('reserved'), {
      foreignKey: 'ticketId',
      as: 'reservedTickets'
    });
  };

  return Model;
};
