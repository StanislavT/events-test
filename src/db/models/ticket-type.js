module.exports = (sequelize, DataTypes) => {
  return sequelize.define('TicketTypes', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING
    }
  }, {
    timestamps: false
  });
};
