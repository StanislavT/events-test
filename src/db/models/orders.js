module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Orders', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      field: 'user_id',
      type: DataTypes.INTEGER
    },
    ticketId: {
      field: 'ticket_id',
      type: DataTypes.INTEGER
    },
    totalPrice: {
      field: 'total_price',
      type: DataTypes.INTEGER
    },
    quantity: {
      type: DataTypes.INTEGER
    },
    paymentToken: {
      field: 'payment_token',
      type: DataTypes.STRING,
    },
    status: {
      type: DataTypes.ENUM,
      values: ['created', 'cancelled', 'expired', 'purchased']
    },
    createdAt: {
      field: 'created_at',
      type: DataTypes.DATE
    },
    updatedAt: {
      field: 'updated_at',
      type: DataTypes.DATE
    }
  }, {
    scopes: {
      reserved: {
        where: {
          status: 'created',
          createdAt: {
            [sequelize.Op.gt]: sequelize.literal("NOW() - INTERVAL '15 minutes'")
          }
        }
      }
    }
  });

  Model.associate = (models) => {
    Model.belongsTo(models.Tickets, {
      foreignKey: 'ticketId',
      as: 'ticket'
    });
  };

  return Model;
};
