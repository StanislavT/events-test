module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Events', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING
    },
    address: {
      type: DataTypes.STRING
    },
    gathering: {
      type: DataTypes.DATE
    }
  }, {
    timestamps: false
  });

  Model.associate = (models) => {
    Model.hasMany(models.Tickets, {
      foreignKey: 'eventId',
      as: 'tickets'
    });
  };

  return Model;
};
