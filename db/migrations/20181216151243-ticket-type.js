module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('TicketTypes', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: Sequelize.STRING
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('TicketTypes', {});
  }
};
