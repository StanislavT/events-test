module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Tickets', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      eventId: {
        field: 'event_id',
        type: Sequelize.INTEGER
      },
      ticketTypeId: {
        field: 'ticket_type_id',
        type: Sequelize.INTEGER
      },
      option: {
        type: Sequelize.ENUM,
        values: ['multiple', 'avoid one', 'altogether']
      },
      quantity: {
        type: Sequelize.INTEGER
      },
      price: {
        type: Sequelize.INTEGER
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Tickets', {});
  }
};
