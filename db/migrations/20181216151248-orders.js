module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Orders', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      userId: {
        field: 'user_id',
        type: Sequelize.INTEGER
      },
      ticketId: {
        field: 'ticket_id',
        type: Sequelize.INTEGER
      },
      totalPrice: {
        field: 'total_price',
        type: Sequelize.INTEGER
      },
      quantity: {
        type: Sequelize.INTEGER
      },
      paymentToken: {
        field: 'payment_token',
        type: Sequelize.STRING,
      },
      status: {
        type: Sequelize.ENUM,
        values: ['created', 'cancelled', 'expired', 'purchased',]
      },
      createdAt: {
        field: 'created_at',
        type: Sequelize.DATE
      },
      updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE
      }
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Orders', {});
  }
};
