module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Tickets', [{
      id: 1,
      event_id: 1,
      ticket_type_id: 1,
      option: 'multiple',
      quantity: 317,
      price: 2500
    }, {
      id: 2,
      event_id: 1,
      ticket_type_id: 2,
      option: 'multiple',
      quantity: 254,
      price: 1700
    }, {
      id: 3,
      event_id: 1,
      ticket_type_id: 3,
      option: 'avoid one',
      quantity: 54,
      price: 1800
    }, {
      id: 4,
      event_id: 2,
      ticket_type_id: 4,
      option: 'altogether',
      quantity: 50,
      price: 900
    }, {
      id: 5,
      event_id: 3,
      ticket_type_id: 1,
      option: 'multiple',
      quantity: 100,
      price: 1300
    }, {
      id: 6,
      event_id: 4,
      ticket_type_id: 4,
      option: 'avoid one',
      quantity: 50,
      price: 700
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Tickets', null, {});
  }
};
