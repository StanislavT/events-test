module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Orders', [{
      user_id: 11,
      ticket_id: 2,
      total_price: 34000,
      quantity: 20,
      status: 'purchased',
      created_at: '2018-12-16 20:34:21+01',
      updated_at: '2018-12-16 20:36:48+01'
    }, {
      user_id: 127,
      ticket_id: 1,
      total_price: 5000,
      quantity: 2,
      status: 'cancelled',
      created_at: '2018-12-17 00:32:17+01',
      updated_at: '2018-12-17 00:33:43+01'
    }, {
      user_id: 1111,
      ticket_id: 6,
      total_price: 2100,
      quantity: 3,
      status: 'expired',
      created_at: '2018-12-11 17:21:56+01',
      updated_at: '2018-12-12 01:00:48+01'
    }, {
      user_id: 21,
      ticket_id: 6,
      total_price: 2100,
      quantity: 3,
      status: 'created',
      created_at: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Orders', null, {});
  }
};
