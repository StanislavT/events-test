module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Events', [{
      id: 1,
      name: 'London Symphony Orchestra in NFM',
      address: 'pl. Wolności 1, Wrocław',
      gathering: '2019-01-16 20:00:00'
    }, {
      id: 2,
      name: 'Halka',
      address: 'ul. Świdnicka 35, Wrocław',
      gathering: '2018-12-10 11:30:00'
    }, {
      id: 3,
      name: 'The Metamorphosis of Książ Castle',
      address: 'ul. Piastów Śląskich 1, Wałbrzych',
      gathering: '2018-12-30 10:00:00'
    }, {
      id: 4,
      name: 'Historic Lviv Relief Panorama',
      address: 'ul. Wystawowa 1, Wrocław',
      gathering: '2018-12-21 19:30:00'
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Events', null, {});
  }
};
