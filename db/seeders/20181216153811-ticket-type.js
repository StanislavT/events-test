module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('TicketTypes', [{
      id: 1,
      name: 'Amphitheater'
    }, {
      id: 2,
      name: 'Balcony'
    }, {
      id: 3,
      name: 'Box'
    }, {
      id: 4,
      name: 'Free'
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('TicketTypes', null, {});
  }
};
