const app = require('./src/api');

app.start()
.catch((err) => {
  process.stderr.write(String(err));
  process.exit(1);
});
